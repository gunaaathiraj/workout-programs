#include<stdio.h>

int main()
{

	int principle, rate, time, interest;

	printf("Enter principle amount: ");
	scanf("%d", &principle);
	printf("Enter rate per annum: ");
	scanf("%d", &rate);
	printf("Enter time: ");
	scanf("%d", &time);

	interest = (principle * rate * time)/100;

	printf("Principle amount: %d\n", principle);
	printf("Rate: %d%%\n", rate);
	printf("Time: %d years\n", time);
	printf("Simple interest: %d\n", interest);

	return 0;

}
